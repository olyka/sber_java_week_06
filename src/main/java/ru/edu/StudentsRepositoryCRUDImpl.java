package ru.edu;

import ru.edu.model.Student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class StudentsRepositoryCRUDImpl implements StudentsRepositoryCRUD {

    /**
     * Insert student's info to table.
     */
    public static final String SQL_INSERT_STUDENT =
            "INSERT INTO STUDENTS "
                    + "(id, "
                    + "first_name, "
                    + "last_name,"
                    + "birth_date,"
                    + "is_graduated, "
                    + "average_grade)"
                    + "VALUES(?, ?, ?, ?, ?, ?)";

    /**
     * Select student's info from table.
     */
    public static final String SQL_SELECT_FROM_STUDENTS =
            "SELECT * FROM STUDENTS";

    /**
     * Delete students from table.
     */
    public static final String SQL_DELETE_FROM_STUDENTS =
            "DELETE FROM STUDENTS";

    /**
     * Update student's info.
     */
    public static final String SQL_UPDATE_STUDENT =
            "UPDATE STUDENTS SET "
                    + "first_name = ?, "
                    + "last_name = ?, "
                    + "birth_date = ?, "
                    + "is_graduated = ?, "
                    + "average_grade = ? ";

    /**
     * Select student's info from table.
     */
    public static final String SQL_WHERE_ID =
            " WHERE id = ?";

    /**
     * Connection to DB.
     */
    private final Connection connection;

    /**
     * Constructor from connection.
     * @param conn Connection to Database
     */
    public StudentsRepositoryCRUDImpl(final Connection conn) {
        this.connection = conn;
    }

    /**
     * Создание записи в БД.
     * id у student должен быть null, иначе требуется вызов update.
     * id генерируем через UUID.randomUUID()
     *
     * @param student Student object
     * @return сгенерированный UUID
     */
    @Override
    public UUID create(final Student student) {

        if (student.getId() != null) {
            update(student);
            return student.getId();
        }

        UUID id = UUID.randomUUID();

        int insertedRows = insertStudent(
                id.toString(),
                student.getFirstName(),
                student.getLastName(),
                student.getBirthDate(),
                student.isGraduated(),
                student.getAverageGrade());

        if (insertedRows == 0) {
            throw new IllegalStateException("Student info was not inserted.");
        }

        student.setId(id);
        return id;

    }

    /**
     * Insert student's info to table.
     * @param values Values to insert in required order
     * @return Result of operation
     */
    private int insertStudent(final Object... values) {

        try (PreparedStatement statement =
                     connection.prepareStatement(SQL_INSERT_STUDENT)) {

            for (int i = 0; i < values.length; i++) {
                statement.setObject((i + 1), values[i]);
            }

            return statement.executeUpdate();

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

    }


    /**
     * Select student's info from table.
     *
     * @param id Student's ID
     * @return Student object
     */
    @Override
    public Student selectById(final UUID id) {

        List<Student> query = selectStudents(
                SQL_SELECT_FROM_STUDENTS + SQL_WHERE_ID,
                 id.toString());

        if (query.isEmpty()) {
            return null;
        }

        if (query.size() > 1) {
            throw new IllegalStateException(
                query.size() + " rows with ID = " + id
                        + " was founded in student's table.");
        }

        return query.get(0);

    }

    /**
     * Select student's info from table by ID.
     * @param sql String with SQL query
     * @param values required parameters for SQL query
     * @return List of Student Objects
     */
    private List<Student> selectStudents(
            final String sql,
            final Object... values) {

        try (PreparedStatement statement =
                     connection.prepareStatement(sql)) {

            for (int i = 0; i < values.length; i++) {
                statement.setObject(i + 1, values[i]);
            }

            ResultSet resultSet = statement.executeQuery();
            List<Student> result = new ArrayList<>();
            while (resultSet.next()) {
                result.add(mapStudent(resultSet));
            }
            return result;

        } catch (SQLException e) {
            throw new IllegalStateException();
        }

    }

    /**
     * Map student's ResultSet to Student's object.
     * @param resultSet from select from table query
     * @return Student object
     * @throws SQLException SQL Exception
     */

    private Student mapStudent(final ResultSet resultSet) throws SQLException {

        Student student = new Student();
        student.setId(UUID.fromString(resultSet.getString("id")));
        student.setFirstName(resultSet.getString("first_name"));
        student.setLastName(resultSet.getString("last_name"));
        student.setBirthDate(resultSet.getDate("birth_date"));
        student.setGraduated(resultSet.getBoolean("is_graduated"));
        student.setAverageGrade(resultSet.getDouble("average_grade"));

        return student;

    }


    /**
     * Get all students from table.
     *
     * @return List of Student objects
     */
    @Override
    public List<Student> selectAll() {

        List<Student> query = selectStudents(SQL_SELECT_FROM_STUDENTS);

        if (query.isEmpty()) {
            return null;
        }

        return query;

    }

    /**
     * Update student in table.
     *
     * @param student Student object
     * @return number of updated rows
     */
    @Override
    public int update(final Student student) {

        try {
            if (selectById(student.getId()) == null) {
                    throw new IllegalStateException(
                            "There is no student with ID = "
                                    + student.getId() + " in student's table.");
            }
        } catch (IllegalStateException e) {
            throw new IllegalStateException(e);
        }

        return updateOrDeleteStudent(
                SQL_UPDATE_STUDENT + SQL_WHERE_ID,
                student.getFirstName(),
                student.getLastName(),
                student.getBirthDate(),
                student.isGraduated(),
                student.getAverageGrade(),
                student.getId().toString());

    }

     /**
     * Delete list of students from table.
     *
     * @param idList List of sStudents IDs
     * @return number of deleted rows
     */
    @Override
    public int remove(final List<UUID> idList) {

        int counter = 0;
        for (UUID uuid : idList) {
            counter += remove(uuid);
        }
        return counter;

    }

    /**
     * Remove student with known id.
     *
     * @param uuid Student's ID
     * @return number of deleted rows
     */
    public int remove(final UUID uuid) {

        return updateOrDeleteStudent(
                SQL_DELETE_FROM_STUDENTS + SQL_WHERE_ID,
                uuid);

    }


    /**
     * Remove all students from table.
     *
     * @return количество удаленных записей
     */
    public int removeAll() {

        return updateOrDeleteStudent(SQL_DELETE_FROM_STUDENTS);

    }


    /**
     * Update student's info or delete student(s) from table.
     * @param sql String with SQL query
     * @param values required parameters for SQL query
     * @return List of Student Objects
     */
    private int updateOrDeleteStudent(
            final String sql,
            final Object... values) {

        try (PreparedStatement statement =
                     connection.prepareStatement(sql)) {

            for (int i = 0; i < values.length; i++) {
                statement.setObject(i + 1, values[i]);
            }

            return statement.executeUpdate();

        } catch (SQLException e) {
            throw new IllegalStateException();
        }

    }

}
